FROM samtechub/bb-xvfb-headless:latest

MAINTAINER Adam Page <adam.page@samtec.com>

ARG APP_DIR

RUN pip3.5 install twine wheel tox virtualenv
RUN mkdir -p ${APP_DIR}

# ENTRYPOINT ["tox"]

CMD ["tox"]
