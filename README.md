# ![Virtuin Logo](https://s3.amazonaws.com/virtuin-static-images/64x64.png) PyVirtuinTestComm

## Overview

PyVirtuinTestComm provides utilities for broadcasting, direct, etc communication over RabbitMQ. [Node.js](https://bitbucket.org/samteccmd/virtuintestcomm#readme) version also available.

The following classes are provided:

* __VirtuinTestPublisher__: Used by active running test to update status and results to subscribers.

* __VirtuinTestSubscriber__: Used by any process wanting to receive test status/result updates.

* __VirtuinTestViewClient__: Used by test view (e.g. web-server) to send requests to test view server.

* __VirtuinTestViewServer__: Used primarily by active test to listen for view requests.  


### Installation:

```
pip install pyvirtuintestcomm --upgrade
```

### Testing

```
tox
```

### Examples

##### VirtuinTestPublisher

```python
from pyvirtuintestcomm import VirtuinTestPublisher

pub = VirtuinTestPublisher(
  'DebugStation',
  'DebugTest',
  'DebugUUID'
)
pub.open('localhost')
pub.publish(
  message='Just warming up.',
  error=None,
  customDict=dict(a=1)
);
pub.updateStatus(
  state='FINISHED',
  progress=100
)
pub.publish(
  message='All done.',
  error=None,
  results=[dict(type='scalar', name='Temp', unit='C', value='28')],
  customDict=dict(a=2)
)
pub.close()
```

##### VirtuinTestSubscriber

```python
from pyvirtuintestcomm import VirtuinTestSubscriber

sub = VirtuinTestSubscriber('DebugStation')
sub.open('localhost')
def subscriber(err, data):
    if err:
        print('Received error {0}'.format(err))
    elif data:
        if data['state'] == 'STARTED':
            print('Test started')
        elif data['progress'] == 100 or data['state'] == 'FINISHED':
            print('Test finished')
            sub.unsubscribe()
sub.subscribe(subscriber)
```

##### VirtuinTestViewServer

```python
from pyvirtuintestcomm import VirtuinTestViewServer

viewServer = VirtuinTestViewServer('DebugStation', 'DebugTest')
viewServer.open('localhost')

def listener(err, data):
    if err:
        print('Received error {0}'.format(err))
    elif data and isinstance(data['operation'], str):
        operation = data['operation'].upper()
        if operation == 'FOO':
            print('Doing FOO')
            return dict(success=True, operation='FOO')
        elif operation == 'BAR':
            print('Doing BAR')
            return dict(success=True, operation='BAR')
        elif operation == 'EXIT':  
            print('Exiting')
            viewServer.stopListening()
            viewServer.close()
            return dict(success=True, operation='EXIT')
        else:
            print('Operation unknown');
            return dict(success=False)
viewServer.listen(listener)
```

##### VirtuinTestViewClient

```python
from pyvirtuintestcomm import VirtuinTestViewClient

viewClient = VirtuinTestViewClient('DebugStation', 'DebugTest')
viewClient.open('localhost')
viewClient.write(dict(operation='FOO'))
viewClient.close()
```

## Packaging

New versions are packaged as a wheel and pushed to [pypi.org](https://pypi.org).
BitBucket Pipelines is used to buid, test, stage, & deploy. Refer to the [pipeline configuration](https://bitbucket.org/samteccmd/pyvirtuintestcomm/src/master/bitbucket-pipelines.yml).

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/samteccmd/pyvirtuintestcomm/commits/).

## Authors

* **Adam Page**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
