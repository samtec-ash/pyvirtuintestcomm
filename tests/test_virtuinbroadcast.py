import time
from multiprocessing import Process, Manager
from pyvirtuintestcomm import VirtuinTestPublisher, VirtuinTestSubscriber


class TestVirtuinBroadcast(object):

    def setup_class(self):
        self.pub = VirtuinTestPublisher('PyTestStation', 'FakeTest', '12345')
        self.pub.version = '0.9.0'
        self.pub.open(hostName='localhost')

    def teardown_class(self):
        self.pub.close(tearDown=True)

    def setup_method(self, test_method):
        self.pub.clearStatus()
        self.sub = VirtuinTestSubscriber('PyTestStation')
        self.sub.open()

    def teardown_method(self, test_method):
        self.sub.close()

    def consumeResult(self, attempts=3, delay=0.1):
        for _ in range(attempts):
            time.sleep(delay)
            rst = self.sub.consume()
            if rst:
                return rst
        return None

    def test_statusTransitions(self):

        self.pub.updateStatus(state='STARTED', progress=0)
        self.pub.publish(message="STARTED")
        rst = self.consumeResult()
        assert isinstance(rst, dict)
        assert rst['status']['progress'] == 0
        assert rst['status']['message'] == 'STARTED'
        assert rst['status']['state'] == 'STARTED'

        self.pub.updateStatus(state='RUNNING', progress=5)
        self.pub.publish(message="RUNNING")
        rst = self.consumeResult()
        assert isinstance(rst, dict)
        assert rst['status']['progress'] == 5
        assert rst['status']['message'] == 'RUNNING'
        assert rst['status']['state'] == 'RUNNING'

        self.pub.updateStatus(progress=50)
        self.pub.publish()
        rst = self.consumeResult()
        assert isinstance(rst, dict)
        assert rst['status']['progress'] == 50
        assert rst['status']['message'] == ''
        assert rst['status']['state'] == 'RUNNING'

        self.pub.updateStatus(state='FINISHED', progress=100, passed=True)
        self.pub.publish(message="FINISHED", results={'success': True})
        rst = self.consumeResult()
        assert isinstance(rst, dict)
        assert rst['status']['progress'] == 100
        assert rst['results'] == {'success': True}
        assert rst['status']['message'] == 'FINISHED'
        assert rst['status']['state'] == 'FINISHED'

    def test_subscription(self):

        def singleShotSubsribe(stationName, results):
            sub = VirtuinTestSubscriber(stationName)
            sub.open()

            def subscribeCB(err, rst):
                results.update(rst or {})
                sub.unsubscribe()
                sub.close()
            sub.subscribe(subscribeCB)

        manager = Manager()
        results = manager.dict()
        p = Process(target=singleShotSubsribe, args=('PyTestStation', results))
        p.start()

        self.pub.updateStatus(
            state='STARTED',
            progress=0,
            passed=None
        )
        self.pub.publish(
            message="Message 1",
            error=None,
            results=None
        )
        p.join(0.4)
        time.sleep(0.1)
        if p.is_alive():
            p.terminate()
        results = self.consumeResult()
        assert results == dict(
            status=dict(
                message='Message 1',
                state='STARTED',
                error=None,
                passed=None,
                testUUID='12345',
                progress=0,
                testName='FakeTest'
            ),
            version='0.9.0'
        )
